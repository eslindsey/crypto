#include <iostream>
#include <memory>

#include "include/Crypto.h"
#include "include/CryptoImpl.h"
#include "include/EricCryptoImpl.h"
#include "include/CryptoChain.h"

using namespace std;

int main() {

    CryptoChain cc;

    auto_ptr<Crypto> c1(new EricCryptoImpl);
    auto_ptr<Crypto> c2(new CryptoImpl);
    auto_ptr<Crypto> c3(new CryptoImpl);

    cc.add(c1.get());
    cc.add(c2.get());
    cc.add(c3.get());

    string input = "aaaaaaaaaaaaa";
    string encoded = cc.encode(input);
    string decoded = cc.decode(encoded);

    cout << input << " -> " << encoded << " -> " << decoded << endl;
}
