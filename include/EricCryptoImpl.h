#ifndef ERICCRYPTOIMPL_H
#define ERICCRYPTOIMPL_H

#include "Crypto.h"

/**
 *  CryptoImpl is an example implementation of the Crypto interface
 *  The pure virtual "encode" and "decode" functions of the Crypto
 *  interface are overridden by the CryptoImpl implementation.
 */
class EricCryptoImpl : public Crypto {
    public:
        EricCryptoImpl();
        virtual ~EricCryptoImpl();
        // Override the pure virtual functions from the Crypto class.
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
};

#endif // ERICCRYPTOIMPL_H
