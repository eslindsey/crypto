#include "../include/EricCryptoImpl.h"

#include <string>

using namespace std;

EricCryptoImpl::EricCryptoImpl() {

}

EricCryptoImpl::~EricCryptoImpl() {

}

// Override the pure virtual functions from the Crypto class.

/**
 * "encodes" by returning the input text
 *
 * @param s - the input string
 * @return the same string
 */
string EricCryptoImpl::encode(string s) {
	int n = 0;
	for (std::string::iterator it = s.begin(); it != s.end(); ++it)
		*it = (*it + (++n)) % 256;
    return s;
}

/**
 * "decodes" by returning the input text
 *
 * @param s - the input string
 * @return the same string
 */
string EricCryptoImpl::decode(string s) {
	int n = 0;
	for (std::string::iterator it = s.begin(); it != s.end(); ++it)
		*it = (*it + (256 - ++n)) % 256;
    return s;
}
